from statistics import median

import matplotlib.pyplot as plt

n, m = map(int, input().split())

data = []
medData = []
razn = 0
razni = 0

for i in range(n):
    data.append(list(map(int, input().split())))

plt.imshow(data)
plt.show()

for row in data:
    for s in row:
        if s not in medData:
            medData.append(s)

medData.sort()


for i in range(1, len(medData)):
    if medData[i] - medData[i - 1] > razn:
        #print(razn)
        razn = medData[i] - medData[i - 1]
        razni = i


#print(medData[razni], medData[razni - 1])
med = medData[razni]

for i in range(n):
    for j in range(m):
        if data[i][j] >= med:
            data[i][j] = 1
        else:
            data[i][j] = 0

#print(data)

plt.imshow(data)
plt.show()

colums = []
rows = []
column = 9999999999999999999
row = 99999999999999999

grid = data[0][0]
info = abs(grid - 1)

for i in range(n):
    last = info
    for j in range(m):
        if data[i][j] == grid and last == info:
            if j not in colums:
                colums.append(j)
        last = data[i][j]

colums.sort()

for i in range(1, len(colums)):
    if colums[i] - colums[i-1] < column:
        column = colums[i] - colums[i-1]

for j in range(m):
    last = info
    for i in range(n):
        if data[i][j] == grid and last == info:
            if i not in rows:
                rows.append(i)
        last = data[i][j]

rows.sort()

#print(colums)
#print(rows)

for i in range(1, len(rows)):
    if rows[i] - rows[i-1] < row:
        row = rows[i] - rows[i-1]


print(n // row, m // column)