def count_pos(num):
    if num >= 512:
        return 9
    if num >= 256:
        return 8
    if num >= 128:
        return 7
    if num >= 64:
        return 6
    if num >= 32:
        return 5
    if num >= 16:
        return 4
    if num >= 8:
        return 3
    if num >= 4:
        return 2
    if num >= 2:
        return 1
    if num >= 1:
        return 0

def bin_search(num, arr):
    minP = 0
    maxP = len(arr) - 1

    while maxP - minP > 1:
        mid = (minP + maxP)//2
        if num > arr[mid]:
            minP = (minP + maxP)//2
        else:
            maxP = (minP + maxP)//2
    if arr[minP] >= num:
        return arr[minP]
    else:
        return arr[maxP]

k = int(input())
a = list(map(int, input().split()))
total = 0
for ai in a:
    total += ai
variants = []

for i in range(1, 2**k):
    num = i
    summ = 0
    pos = 0
    #print(i)
    while num > 0:
        #print(num % 2, end='=')
        if num % 2 == 1:
            summ += a[count_pos(num)]
        num //= 2
    #print("summ", summ)
    if summ not in variants:
        variants.append(summ)


n = int(input())

variants.sort()
#print(variants)

for i in range(n):
    b1, b2 = map(int, input().split())
    val1 = bin_search(b1, variants)
    if b1 > val1:
        print('-', end='')
        continue
    #print(b1, val1)
    val2 = bin_search(b2, variants)
    if b2 > val2:
        print('-', end='')
        continue
    #print(b2, val2)
    #print("===============")

    if total - val1 >= b2 or total - val2 >= b1:
        print('+', end='')
    else:
        print('-', end='')