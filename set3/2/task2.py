p = int(input())
n = int(input())
f = input()[1]

amm = 0

if f == '1':
    amm = 2
elif f == '2':
    amm = 4
elif f == '3':
    amm = 8
elif f == '4':
    amm = 16
elif f == '5':
    amm = 32

bigLists = (n + amm - 1) // amm

print(bigLists * p)