#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct sinap{
    int num, value;
};

bool comp (sinap i, sinap j) {
  return i.value < j.value;
}

int main(){
    int n, p;
    cin >>n >>p;

    vector<sinap> norm_sinaps;
    for (int i = 0; i < n; i++){
        sinap sinap_struc;
        int a;
        cin >>a;
        sinap_struc.num = i+1;
        sinap_struc.value = a;
        norm_sinaps.push_back(sinap_struc);
    }

    sort(norm_sinaps.begin(), norm_sinaps.end(), comp);

    int m = 0, sum = 0;

    for (int i = n - 1; i >= 0; i--){
        sum += norm_sinaps[i].value;
        m += 1;
        if (sum >= p){
            break;
        }
    }

    int pre_sum = 0;

    for (int i = n - 1; n - m;i--){
        pre_sum = norm_sinaps[i].value;
    }

    vector<int> exus;

    for (int i = 0; i < n - m; i++){
        if (pre_sum + norm_sinaps[i].value >= p){
            break;
        }
        exus.push_back(norm_sinaps[i].num);
    }
    sort(exus.begin(), exus.end());

    cout <<m <<endl;
    cout <<exus.size() <<endl;
    for (int i = 0; i < exus.size(); i++){
        cout <<exus[i] <<" ";
    }
}
