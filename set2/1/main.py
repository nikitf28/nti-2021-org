n = int(input())

sum = 0

info = list(map(float, input().split()))

for i in range(n):
    a = abs(info[i])
    if 0.25*n < i+1 <= 0.75*n:
        sum += a
    else:
        sum += a*0.5

print(sum/n)


