#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

using namespace std;

int main(){
    int n;
    vector<double> info;

    cin >>n;
    for (int i = 0; i < n; i++){
        double a;
        cin >>a;
        info.push_back(a);
    }

    double sum = 0;

    for (int i = 0; i < n; i++){
        double a = abs(info[i]);
        if (0.25 * n < i + 1 && i + 1 <= 0.75 * n){
            sum += a;
        }
        else{
            sum += a * 0.5;
        }
    }
    cout << fixed << setprecision(6) <<sum / n;
}
