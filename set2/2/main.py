n, maxhr, dur = input().split()
n, maxhr, dur = int(n), int(maxhr), int(dur)

sum = 0
num = 0

rates = list(map(int, input().split()))

ln = 0

for rate in rates:
    if rate >= maxhr*0.8:
        ln += 1
    else:
        if ln > 0:
            if ln >= dur:
                num += 1
                sum += ln
            ln = 0


if ln > 0:
    if ln >= dur:
        num += 1
        sum += ln
        ln = 0

print(num, sum)